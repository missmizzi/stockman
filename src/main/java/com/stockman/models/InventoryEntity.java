package com.stockman.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * //https://www.journaldev.com/2882/hibernate-tutorial-for-beginners
 * 
 * Created by yanic on 16/12/2018.
 */

@Entity
@Table(name="inventory",
        uniqueConstraints={@UniqueConstraint(columnNames={"id"})})
public class InventoryEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false, unique=true, length=11)
    private int id;

    @Column(name="name", length=100, nullable=false)
    private String name;

    @Column(name="name", nullable=false)
    private int quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
