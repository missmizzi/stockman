package com.stockman.models;
/**
 * A simple POJO for the inventory
 *
 * Created by yanic on 09/12/2018.
 */
public class Inventory {
    private String name;
    private int quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Inventory(String name, int quantity){
        this.name = name;
        this.quantity = quantity;
    }
}
