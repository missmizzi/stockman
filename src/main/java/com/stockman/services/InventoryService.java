package com.stockman.services;

import com.stockman.models.Inventory;

import java.util.List;

/**
 * Created by yanic on 09/12/2018.
 */
public interface InventoryService {
    List<Inventory> loadInventory(String fileName);
}
