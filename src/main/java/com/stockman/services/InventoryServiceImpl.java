package com.stockman.services;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.stockman.models.Inventory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by yanic on 09/12/2018.
 */
@Service
public class InventoryServiceImpl implements  InventoryService {
    private static final String NAME_HEADER = "name";
    private static final String QUANTITY_HEADER = "qty";

    public List<Inventory> loadInventory(String fileName) {
        ArrayList<Inventory> inventories = new ArrayList<Inventory>();
        try {
            //Even though it says with comments... this ignores the comments
            CsvSchema schema = CsvSchema.emptySchema().withHeader().withComments();
            CsvMapper mapper = new CsvMapper();
            File csvFile = new ClassPathResource(fileName).getFile();
            MappingIterator<Map<String,String>> it = mapper.readerFor(Map.class)
                    .with(schema)
                    .readValues(csvFile);
            while (it.hasNext()) {
                Map<String,String> rowAsMap = it.next();
                Inventory inventory = new Inventory(rowAsMap.get(NAME_HEADER).trim(),Integer.parseInt(rowAsMap.get(QUANTITY_HEADER).trim()));
                inventories.add(inventory);
            }
        } catch (Exception e) {
            System.out.println("Error occurred while loading object list from file " + fileName);
            return Collections.emptyList();
        }
        return inventories;
    }
}
