package com.stockman.controllers;

import com.stockman.models.Inventory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by yanic on 09/12/2018.
 */
@Component
public class InventoryViewer {

    private static String HEADER_DELIMETER = "|         Product Name                             |      Available Qty       |";
    private static String INVENTORY_HEADERS_DELIMETER = "| NAME                                             | QTY                      |";
    private static String ROW_DELIMETER = "+--------------------------------------------------+--------------------------+";
    private static String TABLE_DELIMETER = "+==================================================+==========================+";
    private static String INVENTORY_DELIMETER = "| %s                                               | %d                       |";

    private static String NEW_LINE_SEPARATOR = "line.separator";

    /**
     * Displays the inventory in a very fancy way
     *
     * @param inventories
     * @return
     */
    public String displayInventory(List<Inventory> inventories){
        StringBuilder myStringBuilder = new StringBuilder();
        myStringBuilder.append(TABLE_DELIMETER);
        myStringBuilder.append(System.getProperty(NEW_LINE_SEPARATOR));
        myStringBuilder.append(HEADER_DELIMETER);
        myStringBuilder.append(System.getProperty(NEW_LINE_SEPARATOR));
        myStringBuilder.append(TABLE_DELIMETER);
        myStringBuilder.append(System.getProperty(NEW_LINE_SEPARATOR));

        for(int i = 0; i < inventories.size() - 1; i++){
            Inventory thisInventory = inventories.get(i);
           myStringBuilder.append(appendInventories(thisInventory));
            myStringBuilder.append(ROW_DELIMETER);
            myStringBuilder.append(System.getProperty(NEW_LINE_SEPARATOR));
        }

        Inventory thisInventory = inventories.get(inventories.size() - 1);
        myStringBuilder.append(appendInventories(thisInventory));
        myStringBuilder.append(TABLE_DELIMETER);

        return myStringBuilder.toString();
    }

    private StringBuilder appendInventories(Inventory thisInventory){
        StringBuilder myStringBuilder = new StringBuilder();
        myStringBuilder.append(INVENTORY_HEADERS_DELIMETER);
        myStringBuilder.append(System.getProperty(NEW_LINE_SEPARATOR));
        myStringBuilder.append(String.format(INVENTORY_DELIMETER, thisInventory.getName(),thisInventory.getQuantity()));
        myStringBuilder.append(System.getProperty(NEW_LINE_SEPARATOR));

        return myStringBuilder;
    }

}
