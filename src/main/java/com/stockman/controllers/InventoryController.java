package com.stockman.controllers;

import com.stockman.models.Inventory;
import com.stockman.services.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yanic on 16/12/2018.
 */
@Controller
@RequestMapping("/inventory")
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    @RequestMapping(method = RequestMethod.GET)
    public String printHello(ModelMap model) {
        List<Inventory>  inventories =  inventoryService.loadInventory("stockman.csv");
        model.addAttribute("inventoryList",inventories);
        return "index";
    }
}
