package com.stockman;

import com.stockman.config.StockmanConfig;
import com.stockman.controllers.InventoryViewer;
import com.stockman.services.InventoryService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by yanic on 09/12/2018.
 */
public class MainApp {
    private static String FILE_NAME = "stockman.csv";

    public static void main(String[] args){
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(StockmanConfig.class);
        InventoryService inventoryService = ctx.getBean(InventoryService.class);
        InventoryViewer inventoryViewer = ctx.getBean(InventoryViewer.class);
        String csvOutput = inventoryViewer.displayInventory(inventoryService.loadInventory(FILE_NAME));
        System.out.println(csvOutput);
    }
}
