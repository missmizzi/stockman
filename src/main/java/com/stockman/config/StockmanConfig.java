package com.stockman.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Init Spring
 *
 * Created by yanic on 09/12/2018.
 */
@Configuration
@ComponentScan(basePackages = {"com.stockman"})
public class StockmanConfig {
}
