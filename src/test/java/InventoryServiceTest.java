import com.stockman.models.Inventory;
import com.stockman.services.InventoryService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by yanic on 09/12/2018.
 */
public class InventoryServiceTest extends BaseTest{

    @Autowired
    InventoryService inventoryService;

    @Test
    public void test_getInventories_shouldBeRetrievedSuccesfully(){
        List<Inventory> inventories = inventoryService.loadInventory("stockman.csv");
        Assert.assertEquals(3, inventories.size());
        Assert.assertEquals("p1", inventories.get(0).getName());
        Assert.assertEquals(100, inventories.get(0).getQuantity());
        Assert.assertEquals("p2", inventories.get(1).getName());
        Assert.assertEquals(23, inventories.get(1).getQuantity());
        Assert.assertEquals("p3", inventories.get(2).getName());
        Assert.assertEquals(90, inventories.get(2).getQuantity());
    }
}
