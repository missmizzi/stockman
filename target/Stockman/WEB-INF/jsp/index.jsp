<%@ page contentType = "text/html; charset = UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
   <head>
      <title>Hello World</title>
   </head>

   <body>
      <table border="1">
              <tr>
                  <th>Name</th>
                  <th>Quantity</th>
              </tr>
              <c:forEach items="${inventoryList}" var="inventory">
                  <tr>
                      <td>${inventory.name}</td>
                      <td>${inventory.quantity}</td>
                  </tr>
              </c:forEach>
          </table>
   </body>
</html>