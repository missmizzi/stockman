<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
    <title>Stockman</title>
</head>

<body>
    <h2>Inventory</h2>

    <table border="1">
        <tr>
            <th>Name</th>
            <th>Quantity</th>
        </tr>
        <c:forEach items="${inventoryList}" var="inventory">
            <tr>
                <td>${inventory.name}</td>
                <td>${inventory.quantity}</td>
            </tr>
        </c:forEach>
    </table>

</body>
</html>